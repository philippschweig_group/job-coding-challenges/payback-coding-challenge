//
//  WebView.swift
//  Payback Coding Challenge
//
//  Created by Philipp Schweig on 26.07.22.
//

import SwiftUI
import WebKit

struct WebView: UIViewRepresentable {
 
    let url: URL
 
    func makeUIView(context: Context) -> WKWebView {
        return WKWebView()
    }
 
    func updateUIView(_ webView: WKWebView, context: Context) {
        let request = URLRequest(url: url)
        webView.load(request)
    }
}

struct WebView_Previews: PreviewProvider {
    static var previews: some View {
        WebView(url: URL(string: "https://www.payback.de")!)
    }
}
