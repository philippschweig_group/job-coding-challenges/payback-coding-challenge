//
//  SwiftUIExtensions.swift
//  Payback Coding Challenge
//
//  Created by Philipp Schweig on 26.07.22.
//

import SwiftUI

extension EdgeInsets {
    static func all(all: CGFloat) -> EdgeInsets {
        return EdgeInsets.init(top: all, leading: all, bottom: all, trailing: all)
    }
}
