//
//  Endpoint.swift
//  Payback Coding Challenge
//
//  Created by Philipp Schweig on 26.07.22.
//

import Foundation

protocol Endpoint {
    var scheme: String { get }
    var host: String { get }
    var path: String { get }
    var method: RequestMethod { get }
    var header: [String: String]? { get }
    var body: [String: String]? { get }
}
