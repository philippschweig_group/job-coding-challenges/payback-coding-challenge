//
//  RequestMethod.swift
//  Payback Coding Challenge
//
//  Created by Philipp Schweig on 26.07.22.
//

import Foundation

enum RequestMethod: String {
    case delete = "DELETE"
    case get = "GET"
    case patch = "PATCH"
    case post = "POST"
    case put = "PUT"
}
