//
//  WebsiteTileView.swift
//  Payback Coding Challenge
//
//  Created by Philipp Schweig on 27.07.22.
//

import SwiftUI

struct WebsiteTileView: FeedTileView {
    let tile: FeedTile
    
    var content: some View {
        if let url = tile.url {
            WebView(url: url)
                .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .center)
        }
        else {
            Text("No url set!")
        }
    }
}

struct WebsiteTileView_Previews: PreviewProvider {
    static var previews: some View {
        let tile = FeedTile(
            type: .webview,
            headline: "Payback",
            subline: "Coupons, Payment and more.",
            rank: 0,
            url: URL(string: "https://www.payback.de")
        )
        
        WebsiteTileView(tile: tile)
    }
}
