//
//  ImageTileView.swift
//  Payback Coding Challenge
//
//  Created by Philipp Schweig on 27.07.22.
//

import SwiftUI

struct ImageTileView: FeedTileView {
    let tile: FeedTile
    
    var content: some View {
        if let url = tile.url {
            AsyncImage(url: url) { image in
                image
            } placeholder: {
                ProgressView()
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .center)
        }
        else {
            Text("No url set!")
        }
    }
}

struct ImageTileView_Previews: PreviewProvider {
    static var previews: some View {
        let tile = FeedTile(
            type: .image,
            headline: "Tim Cook",
            subline: "CEO of Apple Inc.",
            rank: 0,
            url: URL(string: "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d5/Tim_Cook_2009_cropped.jpg/220px-Tim_Cook_2009_cropped.jpg")
        )
        
        ImageTileView(tile: tile)
    }
}
