//
//  FeedTileView.swift
//  Payback Coding Challenge
//
//  Created by Philipp Schweig on 27.07.22.
//

import SwiftUI

protocol FeedTileView: View {
    var tile: FeedTile { get }
    
    associatedtype Content : View
    @ViewBuilder var content: Content { get }
}

extension FeedTileView {
    var body: some View {
        VStack(alignment: .leading) {
            if let headline = tile.headline {
                Text(headline)
            }
            content
            if let subline = tile.subline {
                Text(subline)
            }
        }
    }
}

struct FeedTileViewFactory {
    func tileViewForFeedTile(_ feedTile: FeedTile) -> AnyView {
        switch feedTile.type {
        case .image:
            return AnyView(ImageTileView(tile: feedTile))
        case .video:
            return AnyView(VideoTileView(tile: feedTile))
        case .webview:
            return AnyView(WebsiteTileView(tile: feedTile))
        default:
            return AnyView(Text("FeedTileView not implemented"))
        }
    }
}
