//
//  VideoTileView.swift
//  Payback Coding Challenge
//
//  Created by Philipp Schweig on 27.07.22.
//

import SwiftUI
import AVKit

struct VideoTileView: FeedTileView {
    let tile: FeedTile
    
    var content: some View {
        if let url = tile.url {
            VideoPlayer(player: AVPlayer(url: url))
                .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .center)
        }
        else {
            Text("No url set!")
        }
    }
}

struct VideoTileView_Previews: PreviewProvider {
    static var previews: some View {
        let tile = FeedTile(
            type: .video,
            headline: "Cartoon",
            subline: nil,
            rank: 0,
            url: URL(string: "https://sample-videos.com/video123/mp4/720/big_buck_bunny_720p_1mb.mp4")
        )
        
        VideoTileView(tile: tile)
    }
}
