//
//  FeedListView.swift
//  Payback Coding Challenge
//
//  Created by Philipp Schweig on 22.07.22.
//

import SwiftUI

struct FeedListView: View {
    let viewModel: FeedListViewModel
    
    init(viewModel: FeedListViewModel) {
        self.viewModel = viewModel
    }
    
    var body: some View {
        List() {
            ForEach(viewModel.tiles) {tile in
                FeedTileViewFactory()
                    .tileViewForFeedTile(tile)
                    .listRowInsets(EdgeInsets.all(all: 14))
                    .frame(maxWidth: .infinity, minHeight: 200, alignment: .leading)
            }
        }
        .listStyle(PlainListStyle())
    }
}

struct FeedListView_Previews: PreviewProvider {
    static var previews: some View {
        FeedListView(viewModel: FeedListViewModel())
    }
}
