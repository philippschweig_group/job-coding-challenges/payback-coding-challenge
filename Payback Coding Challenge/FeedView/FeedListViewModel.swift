//
//  FeedListViewModel.swift
//  Payback Coding Challenge
//
//  Created by Philipp Schweig on 27.07.22.
//

import Foundation

struct FeedListViewModel {
    var tiles: [FeedTile]
    
    init() {
        tiles = [
            FeedTile(
                type: .image,
                headline: "Tim Cook",
                subline: "CEO of Apple Inc.",
                rank: 0,
                url: URL(string: "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d5/Tim_Cook_2009_cropped.jpg/220px-Tim_Cook_2009_cropped.jpg")
            ),
            FeedTile(
                type: .video,
                headline: "Cartoon",
                subline: nil,
                rank: 0,
                url: URL(string: "https://sample-videos.com/video123/mp4/720/big_buck_bunny_720p_1mb.mp4")
            ),
            FeedTile(
                type: .webview,
                headline: "Payback",
                subline: "Coupons, Payment and more.",
                rank: 0,
                url: URL(string: "https://www.payback.de")
            )
        ]
    }
}
