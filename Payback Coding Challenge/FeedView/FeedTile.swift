//
//  FeedTile.swift
//  Payback Coding Challenge
//
//  Created by Philipp Schweig on 22.07.22.
//

import Foundation

enum FeedTileType {
    case video
    case image
    case webview
    case shoppinglist
}

struct FeedTile: Identifiable {
    let id = UUID()
    let type: FeedTileType
    
    let headline: String?
    let subline: String?
    let rank: Int
    let url: URL?
}
