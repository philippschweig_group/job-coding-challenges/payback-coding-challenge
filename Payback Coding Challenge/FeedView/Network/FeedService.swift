//
//  FeedService.swift
//  Payback Coding Challenge
//
//  Created by Philipp Schweig on 28.07.22.
//

import Foundation

protocol FeedService {
    func getFeed() async -> Result<FeedResponse, RequestError>
}
