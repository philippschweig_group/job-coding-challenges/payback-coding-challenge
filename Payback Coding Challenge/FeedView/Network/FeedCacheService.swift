//
//  FeedCacheService.swift
//  Payback Coding Challenge
//
//  Created by Philipp Schweig on 27.07.22.
//

import Foundation

struct FeedCacheService: FeedService {
    func getFeed() async -> Result<FeedResponse, RequestError> {
        return Result.failure(.unknown)
    }
}
