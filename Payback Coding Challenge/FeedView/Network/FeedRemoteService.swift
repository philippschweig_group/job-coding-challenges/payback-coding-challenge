//
//  FeedRemoteService.swift
//  Payback Coding Challenge
//
//  Created by Philipp Schweig on 26.07.22.
//

import Foundation

enum FeedEndpoint {
    case feed
}

// https://firebasestorage.googleapis.com/v0/b/payback- test.appspot.com/o/feed.json?alt=media&token=3b3606dd-1d09-4021-a013-a30e958ad930
extension FeedEndpoint: Endpoint {
    var scheme: String {
        return "https"
    }
    
    var host: String {
        return "firebasestorage.googleapis.com"
    }
    
    var path: String {
        return "v0/b/payback- test.appspot.com/o/feed.json?alt=media&token=3b3606dd-1d09-4021-a013-a30e958ad930"
    }
    
    var method: RequestMethod {
        return .get
    }
    
    var header: [String : String]? {
        return nil;
    }
    
    var body: [String : String]?  {
        return nil;
    }
}

struct FeedRemoteService: HTTPClient, FeedService {
    func getFeed() async -> Result<FeedResponse, RequestError> {
        return await sendRequest(endpoint: FeedEndpoint.feed, responseBody: FeedResponse.self)
    }
}
