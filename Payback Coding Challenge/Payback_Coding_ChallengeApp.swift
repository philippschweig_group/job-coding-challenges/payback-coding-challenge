//
//  Payback_Coding_ChallengeApp.swift
//  Payback Coding Challenge
//
//  Created by Philipp Schweig on 22.07.22.
//

import SwiftUI

@main
struct Payback_Coding_ChallengeApp: App {
    var body: some Scene {
        WindowGroup {
            FeedListView(viewModel: FeedListViewModel())
        }
    }
}
